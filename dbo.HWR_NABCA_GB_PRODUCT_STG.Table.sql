USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_NABCA_GB_PRODUCT_STG]    Script Date: 9/28/2019 6:52:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_NABCA_GB_PRODUCT_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_NABCA_GB_PRODUCT_STG](
	[SEQUENCE_NUMBER] [varchar](50) NULL,
	[SALES_DATE] [date] NULL,
	[STATE_NO] [int] NULL,
	[STATE_CODE] [varchar](50) NULL,
	[STATE_ABREV] [varchar](50) NULL,
	[CSC] [varchar](50) NULL,
	[COM_CODE] [varchar](50) NULL,
	[COM_CODE_NAME] [varchar](50) NULL,
	[CLASS_NO] [varchar](50) NULL,
	[CLASS_ABREV] [varchar](50) NULL,
	[BRAND_NO] [varchar](50) NULL,
	[BRAND_ABREV] [varchar](50) NULL,
	[BRAND_FULL] [varchar](50) NULL,
	[SIZE_NO] [varchar](50) NULL,
	[SIZE_DESC] [varchar](50) NULL,
	[PACK_SIZE] [int] NULL,
	[UPC_CODE] [varchar](50) NULL,
	[SCC_CODE] [varchar](50) NULL,
	[VENDOR_NO] [varchar](50) NULL,
	[VENDOR_ABREV] [varchar](50) NULL,
	[DISCUS_VENDOR_NO] [varchar](50) NULL,
	[DISCUS_VENDOR_ABREV] [varchar](50) NULL,
	[IMP_DOM_FLAG] [varchar](50) NULL,
	[IMP_DOM_DESC] [varchar](50) NULL,
	[AGE] [varchar](50) NULL,
	[PROOF] [int] NULL,
	[BEVERAGE_TYPE] [varchar](50) NULL,
	[SIZE_LITER] [float] NULL,
	[STATE_NAME] [varchar](50) NULL,
	[MH_BRAND] [varchar](50) NULL,
	[MH_MARQUE] [varchar](50) NULL,
	[COMMON_CODE] [varchar](50) NULL,
	[MAJOR_CATEGORY] [varchar](50) NULL,
	[MINOR_CATEGORY1] [varchar](50) NULL,
	[MINOR_CATEGORY2] [varchar](50) NULL,
	[BRAND_FAMILY] [varchar](50) NULL,
	[CLASS] [varchar](50) NULL,
	[GB_CLASS] [varchar](50) NULL,
	[GB_CLASS_GROUP] [varchar](50) NULL,
	[SPIRITS_OR_WINES] [varchar](50) NULL,
	[IWSR_CATEGORY] [varchar](50) NULL,
	[MODIFIED_DATE] [datetime] NULL,
	[MODIFIED_BY] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_NABCA_GB_PRODUCT_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
