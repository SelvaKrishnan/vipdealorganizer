USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_NABCA_SAM_TIME_DIM1]    Script Date: 9/28/2019 6:52:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_NABCA_SAM_TIME_DIM1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_NABCA_SAM_TIME_DIM1](
	[Time_ID] [int] NOT NULL,
	[Time_Date] [datetime2](7) NOT NULL,
	[Time_Day] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[Month_Number] [int] NOT NULL,
	[Time_Business_Day_Ind] [int] NOT NULL,
	[Month_Name] [nvarchar](50) NOT NULL,
	[Month_Short_Name] [nvarchar](50) NOT NULL,
	[YTD] [nvarchar](50) NOT NULL,
	[PRIOR_YTD] [nvarchar](50) NOT NULL,
	[YTD_1] [nvarchar](50) NOT NULL,
	[PRIOR_YTD_1] [nvarchar](50) NOT NULL,
	[R12] [nvarchar](50) NOT NULL,
	[PRIOR_R12] [nvarchar](50) NOT NULL,
	[R12_1] [nvarchar](50) NOT NULL,
	[PRIOR_R12_1] [nvarchar](50) NOT NULL,
	[R6] [nvarchar](50) NOT NULL,
	[PRIOR_R6] [nvarchar](50) NOT NULL,
	[R3] [nvarchar](50) NOT NULL,
	[PRIOR_R3] [nvarchar](50) NOT NULL,
	[R1] [nvarchar](50) NOT NULL,
	[PRIOR_R1] [nvarchar](50) NOT NULL
) ON [PRIMARY]
END
GO
UPDATE STATISTICS [dbo].[HWR_NABCA_SAM_TIME_DIM1] WITH ROWCOUNT = 1212, PAGECOUNT = 19
GO
