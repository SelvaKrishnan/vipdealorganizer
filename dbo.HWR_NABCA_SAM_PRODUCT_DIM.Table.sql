USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_NABCA_SAM_PRODUCT_DIM]    Script Date: 9/28/2019 6:52:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_NABCA_SAM_PRODUCT_DIM]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_NABCA_SAM_PRODUCT_DIM](
	[PRODUCT_KEY] [varchar](50) NULL,
	[BRAND_NO] [varchar](50) NULL,
	[BRAND_ABREV] [varchar](50) NULL,
	[BRAND_FULL] [varchar](50) NULL,
	[BEVERAGE_TYPE] [varchar](50) NULL,
	[SIZE_DESC] [varchar](50) NULL,
	[PACK_SIZE] [int] NULL,
	[COMCODE] [varchar](50) NULL,
	[COM_CNAME ] [varchar](50) NULL,
	[VENDOR_ABREV] [varchar](50) NULL,
	[MAJOR_CATEGORY] [varchar](50) NULL,
	[MINOR_CATEGORY1] [varchar](50) NULL,
	[MINOR_CATEGORY2] [varchar](50) NULL,
	[BRAND_FAMILY] [varchar](50) NULL,
	[CLASS] [varchar](50) NULL,
	[GB_CLASS] [varchar](50) NULL,
	[GB_CLASS_GROUP] [varchar](50) NULL,
	[SPIRITS_OR_WINES] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_NABCA_SAM_PRODUCT_DIM] WITH ROWCOUNT = 529724, PAGECOUNT = 13665
GO
