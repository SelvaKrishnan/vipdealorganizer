USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_DIM_DIST_GEO_STG]    Script Date: 9/28/2019 6:52:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_DIM_DIST_GEO_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_DIM_DIST_GEO_STG](
	[CUSTOMER_TYPE] [varchar](50) NULL,
	[REGION] [varchar](50) NULL,
	[SUB_REGION] [varchar](50) NULL,
	[STATE_DESCIPTION] [varchar](50) NULL,
	[COMPANY] [varchar](50) NULL,
	[SHIP_TO_DESCRIPTION] [varchar](50) NULL,
	[SHIP_TO] [varchar](50) NULL,
	[ADDRESS] [varchar](50) NULL,
	[CITY] [varchar](50) NULL,
	[ZIP_CODE] [varchar](50) NULL,
	[HN_BATTLEGROUND] [varchar](50) NULL,
	[MODIFIED_DATE] [datetime] NULL,
	[MODIFIED_BY] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_DIM_DIST_GEO_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
