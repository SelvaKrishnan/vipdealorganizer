USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_DIM_TIME_STG]    Script Date: 9/28/2019 6:52:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_DIM_TIME_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_DIM_TIME_STG](
	[TIME_ID] [numeric](18, 0) NULL,
	[TIME_DATE] [varchar](50) NULL,
	[TIME_DAY] [varchar](50) NULL,
	[TIME_YEAR] [varchar](50) NULL,
	[TIME_MONTH] [varchar](50) NULL,
	[TIME_MONTH_NAME] [varchar](50) NULL,
	[TIME_MONTH_SHORT_NAME] [varchar](50) NULL,
	[TIME_BUSINESS_DAY_IND] [varchar](50) NULL,
	[MTD] [varchar](50) NULL,
	[PRIOR_MTD] [varchar](50) NULL,
	[MTD_1] [varchar](50) NULL,
	[PRIOR_MTD_1] [varchar](50) NULL,
	[YTD] [varchar](50) NULL,
	[PRIOR_YTD] [varchar](50) NULL,
	[CYTD] [varchar](50) NULL,
	[PRIOR_CYTD] [varchar](50) NULL,
	[R12] [varchar](50) NULL,
	[PRIOR_R12] [varchar](50) NULL,
	[CR12] [varchar](50) NULL,
	[PRIOR_CR12] [varchar](50) NULL,
	[R24] [varchar](50) NULL,
	[PRIOR_R24] [varchar](50) NULL,
	[MODIFIED_DATE] [datetime] NULL,
	[MODIFIED_BY] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_DIM_TIME_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
