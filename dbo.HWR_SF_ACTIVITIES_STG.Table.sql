USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_SF_ACTIVITIES_STG]    Script Date: 9/28/2019 6:52:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_SF_ACTIVITIES_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_SF_ACTIVITIES_STG](
	[DISCUS_BRAND_CODE] [varchar](50) NULL,
	[H1_CODE] [int] NULL,
	[BRAND_FAMILY] [varchar](50) NULL,
	[BRAND_NAME_FULL] [varchar](50) NULL,
	[BRAND_NAME_ABBR] [varchar](50) NULL,
	[VENDOR_DISCUS_CODE] [int] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[ISALLDAYEVENT] [varchar](50) NULL,
	[DATEOFDATA] [varchar](50) NULL,
	[DATEOFDATA_MON] [varchar](50) NULL,
	[DATEOFDATA_YEAR] [varchar](50) NULL,
	[ISPRIVATE] [varchar](50) NULL,
	[SHOWAS] [varchar](50) NULL,
	[ISCHILD] [varchar](50) NULL,
	[ISGROUPEVENT] [varchar](50) NULL,
	[CREATEDDATE] [varchar](50) NULL,
	[CREATEDDATE_MON] [varchar](50) NULL,
	[CREATEDDATE_YEAR] [varchar](50) NULL,
	[ISARCHIVED] [varchar](50) NULL,
	[ISRECURRENCE] [varchar](50) NULL,
	[ISREMINDERSET] [varchar](50) NULL,
	[EVENTSUBTYPE] [varchar](50) NULL,
	[ACCOUNT_ACTIVITY_TYPE] [varchar](50) NULL,
	[MARQUE_DESCRIPTION] [varchar](50) NULL,
	[CONSUMER_ATTENDEES] [float] NULL,
	[JOURNEYPLANNINGEVENT] [varchar](50) NULL,
	[PLACEMENT_GROUP] [varchar](50) NULL,
	[TRADE_ATTENDEES] [float] NULL,
	[EXECUTION_DATE] [varchar](50) NULL,
	[EXECUTION_MON] [varchar](50) NULL,
	[EXECUTION_YEAR] [varchar](50) NULL,
	[SNAME] [varchar](50) NULL,
	[TDLINX_CODE] [varchar](50) NULL,
	[SF_ACCOUNT_OWNER] [varchar](50) NULL,
	[PROFILE] [varchar](50) NULL,
	[MHUSA_CHANNEL] [varchar](50) NULL,
	[REGION] [varchar](50) NULL,
	[PRIMARY_FLAVOR] [varchar](50) NULL,
	[HISTORY_AS_OF_MONTH] [varchar](50) NULL,
	[HISTORY_AS_OF_YEAR] [varchar](50) NULL,
	[H2_L4] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_SF_ACTIVITIES_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
