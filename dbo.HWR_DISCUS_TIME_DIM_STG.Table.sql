USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_DISCUS_TIME_DIM_STG]    Script Date: 9/28/2019 6:52:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_DISCUS_TIME_DIM_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_DISCUS_TIME_DIM_STG](
	[TIME_ID] [int] NULL,
	[TIME_DATE] [date] NULL,
	[TIME_DAY] [int] NULL,
	[TIME_YEAR] [int] NULL,
	[TIME_MONTH] [int] NULL,
	[TIME_BUSINESS_DAY_IND] [int] NULL,
	[TIME_MONTH_NAME] [varchar](50) NULL,
	[TIME_MONTH_SHORT_NAME] [varchar](50) NULL,
	[YTD] [varchar](50) NULL,
	[PRIOR_YTD] [varchar](50) NULL,
	[YTD_1] [varchar](50) NULL,
	[PRIOR_YTD_1] [varchar](50) NULL,
	[R12] [varchar](50) NULL,
	[PRIOR_R12] [varchar](50) NULL,
	[R12_1] [varchar](50) NULL,
	[PRIOR_R12_1] [varchar](50) NULL,
	[R6] [varchar](50) NULL,
	[PRIOR_R6] [varchar](50) NULL,
	[R3] [varchar](50) NULL,
	[PRIOR_R3] [varchar](50) NULL,
	[R1] [varchar](50) NULL,
	[PRIOR_R1] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_DISCUS_TIME_DIM_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
