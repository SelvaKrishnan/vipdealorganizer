USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_DISCUS_BRAND_STG]    Script Date: 9/28/2019 6:52:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_DISCUS_BRAND_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_DISCUS_BRAND_STG](
	[DISCUS_BRAND_CODE] [varchar](50) NULL,
	[H1_CODE] [int] NULL,
	[BRAND_FAMILY] [varchar](50) NULL,
	[BRAND_NAME_FULL] [varchar](50) NULL,
	[BRAND_NAME_ABBR] [varchar](50) NULL,
	[VENDOR_DISCUS_CODE] [int] NULL,
	[FULL_VENDOR_NAME] [varchar](50) NULL,
	[VENDOR_ABBREV] [varchar](50) NULL,
	[MEMBER_FLAG] [varchar](50) NULL,
	[ACTIVE_DATE_YYYY] [varchar](50) NULL,
	[ACTIVE_DATE_MM] [varchar](50) NULL,
	[PROOF] [int] NULL,
	[AGE] [varchar](50) NULL,
	[ACTIVE_FLAG] [varchar](50) NULL,
	[CROSS_REF_NAME] [varchar](50) NULL,
	[H2_CODE] [int] NULL,
	[H1_L1] [varchar](50) NULL,
	[H1_L2] [varchar](50) NULL,
	[H1_L3] [varchar](50) NULL,
	[H1_L4] [varchar](50) NULL,
	[H2_L0] [varchar](50) NULL,
	[H2_L1] [varchar](50) NULL,
	[H2_L2] [varchar](50) NULL,
	[H2_L3] [varchar](50) NULL,
	[H1_L5] [varchar](50) NULL,
	[CROSS_REF_CODE] [varchar](50) NULL,
	[BRAND_CODE] [int] NULL,
	[SECONDARY_FLAVOR_CODE] [int] NULL,
	[SECONDARY_FLAVOR] [varchar](50) NULL,
	[V_1_COMMENT] [varchar](50) NULL,
	[V_2_COMMENT] [varchar](50) NULL,
	[V_3_COMMENT] [varchar](50) NULL,
	[V_4_COMMENT] [varchar](50) NULL,
	[V_5_COMMENT] [varchar](50) NULL,
	[MAJOR_FLAVOR_CODE] [varchar](50) NULL,
	[MAJOR_FLAVOR] [varchar](50) NULL,
	[PRIMARY_FLAVOR_CODE] [varchar](50) NULL,
	[PRIMARY_FLAVOR] [varchar](50) NULL,
	[HISTORY_AS_OF_MONTH] [varchar](50) NULL,
	[HISTORY_AS_OF_YEAR] [varchar](50) NULL,
	[H2_L4] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_DISCUS_BRAND_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
