USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_IRI_PRODUCT]    Script Date: 9/28/2019 6:52:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_IRI_PRODUCT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_IRI_PRODUCT](
	[UPC_13_DIGIT] [varchar](50) NULL,
	[UPC_10_DIGIT] [varchar](50) NULL,
	[CA_MEGA_CATEGORY] [varchar](50) NULL,
	[CA_CATEGORY] [varchar](50) NULL,
	[SUB_CATEGORY] [varchar](50) NULL,
	[SIZE_LITER] [float] NULL,
	[SEGMENT] [varchar](50) NULL,
	[PRICE_FAMILY] [varchar](50) NULL,
	[PRICE_TIER] [varchar](50) NULL,
	[SUPPLIER] [varchar](50) NULL,
	[BRAND_FAMILY] [varchar](100) NULL,
	[BRAND] [varchar](100) NULL,
	[DOMESTIC_VS_IMPORTED] [varchar](50) NULL,
	[COLOR_GROUP] [varchar](50) NULL,
	[FLAVOR_VARIETAL] [varchar](100) NULL,
	[VINTAGE_AGE] [varchar](50) NULL,
	[SIZE] [varchar](50) NULL,
	[PACKAGE] [varchar](50) NULL,
	[FLAVOR] [varchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_IRI_PRODUCT] WITH ROWCOUNT = 91348, PAGECOUNT = 3013
GO
