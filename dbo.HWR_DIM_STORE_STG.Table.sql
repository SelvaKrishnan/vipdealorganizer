USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_DIM_STORE_STG]    Script Date: 9/28/2019 6:52:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_DIM_STORE_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_DIM_STORE_STG](
	[REGION] [varchar](50) NULL,
	[DMA_NAME] [varchar](50) NULL,
	[STATE_DESCRIPTION] [varchar](50) NULL,
	[SCITY] [varchar](50) NULL,
	[ON_OFF_INDICATOR] [varchar](50) NULL,
	[SCHAINDESC] [varchar](50) NULL,
	[MHUSA_SUB_CHANNEL] [varchar](50) NULL,
	[SCHANNEL] [varchar](50) NULL,
	[SSUBCHANNEL] [varchar](50) NULL,
	[ZIP_CODE] [varchar](50) NULL,
	[STDLINXSCD] [varchar](50) NULL,
	[STORE_TYPE] [varchar](50) NULL,
	[SNO] [varchar](50) NULL,
	[STORE_NAME_ADDR] [varchar](200) NULL,
	[HULNAME] [varchar](50) NULL,
	[SGRPNM] [varchar](50) NULL,
	[SWS_CALLEDON_ACCTS] [varchar](50) NULL,
	[CONTROL_CALLEDON_ACCTS] [varchar](50) NULL,
	[BANNER_NAME] [varchar](50) NULL,
	[BANNER_OFF_NEW] [varchar](50) NULL,
	[PROFILES] [varchar](50) NULL,
	[ACCOUNT_OWNER] [varchar](50) NULL,
	[HEAT_ACCOUNT] [varchar](50) NULL,
	[SLONG] [numeric](14, 4) NOT NULL,
	[SLAT] [numeric](14, 4) NOT NULL,
	[CONQUEST_FLAG] [varchar](50) NULL,
	[MULTIPLE_MENTIONS] [varchar](50) NULL,
	[#MENTIONS] [varchar](50) NULL,
	[CONQUEST_MULTIPLE_MENTIONS] [varchar](50) NULL,
	[CONQUEST_#MENTIONS] [varchar](50) NULL,
	[SWINE] [varchar](50) NULL,
	[SBEER] [varchar](50) NULL,
	[SLIQUOR] [varchar](50) NULL,
	[SNAME] [varchar](200) NULL,
	[STORE_STATUS] [varchar](50) NULL,
	[MODIFIED_DATE] [datetime] NULL,
	[MODIFIED_BY] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_DIM_STORE_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
