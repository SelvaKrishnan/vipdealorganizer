USE [Hennessy_WR]
GO
/****** Object:  View [dbo].[HWR_SG_PLACEMENT_ACTUALS]    Script Date: 9/28/2019 6:52:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[HWR_SG_PLACEMENT_ACTUALS]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[HWR_SG_PLACEMENT_ACTUALS]
AS
SELECT        dbo.HWR_DIM_STORE.STATE_DESCRIPTION, dbo.HWR_SF_PLACEMENT.CREATED_BY, dbo.HWR_SF_PLACEMENT.MARQUE_DESCRIPTION AS TARGET_BRAND, dbo.HWR_SF_PLACEMENT.CORE_KPI, 
                         SUM(dbo.HWR_SF_PLACEMENT.NUMBER_OF_LISTINGS) AS PLACEMENTS
FROM            dbo.HWR_SF_PLACEMENT INNER JOIN
                         dbo.HWR_DIM_STORE ON dbo.HWR_SF_PLACEMENT.TDLINX_CODE = dbo.HWR_DIM_STORE.STDLINXSCD
WHERE        (dbo.HWR_SF_PLACEMENT.DATEOFDATA_YEAR = 2019)
GROUP BY dbo.HWR_DIM_STORE.STATE_DESCRIPTION, dbo.HWR_SF_PLACEMENT.CREATED_BY, dbo.HWR_SF_PLACEMENT.MARQUE_DESCRIPTION, dbo.HWR_SF_PLACEMENT.CORE_KPI
' 
GO
IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'HWR_SG_PLACEMENT_ACTUALS', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "HWR_SF_PLACEMENT"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 293
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "HWR_DIM_STORE"
            Begin Extent = 
               Top = 6
               Left = 331
               Bottom = 136
               Right = 622
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'HWR_SG_PLACEMENT_ACTUALS'
GO
IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'HWR_SG_PLACEMENT_ACTUALS', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'HWR_SG_PLACEMENT_ACTUALS'
GO
