USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_ALLOCATION_STG]    Script Date: 9/28/2019 6:52:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_ALLOCATION_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_ALLOCATION_STG](
	[PRODUCT] [varchar](150) NULL,
	[PRODUCT_CODE] [varchar](50) NULL,
	[PRODUCT_DESCRIPTION] [varchar](150) NULL,
	[BRAND] [varchar](50) NULL,
	[QUALITY] [varchar](50) NULL,
	[PLANT_ID] [int] NULL,
	[PLANT] [varchar](50) NULL,
	[REGION_ID] [int] NULL,
	[REGION] [varchar](50) NULL,
	[STATE] [varchar](50) NULL,
	[SHIP_TO_ID] [varchar](50) NULL,
	[SHIP_TO] [varchar](50) NULL,
	[SALES_SECTOR] [varchar](50) NULL,
	[START_DATE] [varchar](50) NULL,
	[START_DATE_MON] [varchar](50) NULL,
	[START_DATE_YEAR] [varchar](50) NULL,
	[END_DATE] [varchar](50) NULL,
	[END_DATE_MON] [varchar](50) NULL,
	[END_DATE_YEAR] [varchar](50) NULL,
	[DEACTIVATED] [varchar](50) NULL,
	[ALLOC_GROUP_ID] [varchar](50) NULL,
	[ALLOC_QTY] [int] NULL,
	[CONSUM_ALLOC_QTY] [int] NULL,
	[OPEN_ALLOC_QTY] [int] NULL,
	[ALLOCATION_ID] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_ALLOCATION_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
