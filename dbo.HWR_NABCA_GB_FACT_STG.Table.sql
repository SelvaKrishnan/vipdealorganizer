USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_NABCA_GB_FACT_STG]    Script Date: 9/28/2019 6:52:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_NABCA_GB_FACT_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_NABCA_GB_FACT_STG](
	[TIME_ID] [varchar](50) NULL,
	[SALES_ID] [varchar](50) NULL,
	[DVL_ID] [varchar](50) NULL,
	[SEQUENCE_NUMBER] [varchar](50) NULL,
	[STATE_NO] [int] NULL,
	[DATE OF DATA] [varchar](50) NULL,
	[NINE_LITER_CASES] [float] NULL,
	[STANDARD_CASES] [float] NULL,
	[SZ_BOTTLE_SOLD_QTY] [float] NULL,
	[PR_BOTTLE_SOLD_QTY] [float] NULL,
	[SHELF_PRICE] [float] NULL,
	[RETAIL_PRICE] [float] NULL,
	[SHELF_DOLLAR_VOLUME] [float] NULL,
	[RETAIL_DOLLAR_VOLUME] [float] NULL,
	[STATE_ABREV] [varchar](50) NULL,
	[SHELF_DOLLAR_VOL_ADJ] [float] NULL,
	[MODIFIED_DATE] [datetime] NULL,
	[MODIFIED_BY] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_NABCA_GB_FACT_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
