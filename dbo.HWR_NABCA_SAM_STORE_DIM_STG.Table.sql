USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_NABCA_SAM_STORE_DIM_STG]    Script Date: 9/28/2019 6:52:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_NABCA_SAM_STORE_DIM_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_NABCA_SAM_STORE_DIM_STG](
	[STORE_LIC_KEY] [varchar](50) NULL,
	[NABCA_STORE_NAME] [varchar](50) NULL,
	[NABCA_STATE_ABREV] [varchar](50) NULL,
	[MCOUNTYNM] [varchar](50) NULL,
	[STORESTATUS] [varchar](50) NULL,
	[SCITY] [varchar](50) NULL,
	[SCHANNEL] [varchar](50) NULL,
	[SSUBCHANNEL] [varchar](50) NULL,
	[SCHAINDESC] [varchar](50) NULL,
	[SLAT] [float] NULL,
	[SLONG] [float] NULL,
	[SGRPNM] [varchar](50) NULL,
	[SFOODPRIMARYTYPE] [varchar](50) NULL,
	[STATE_DESCRIPTION] [varchar](50) NULL,
	[REGION] [varchar](50) NULL,
	[HULNAME] [varchar](50) NULL,
	[STORE_NAME_ADDR_TDLINX] [varchar](150) NULL,
	[ZIP_CODE] [varchar](50) NULL,
	[BANNER_NAME] [varchar](50) NULL,
	[ON_OFF_INDICATOR] [varchar](50) NULL,
	[BANNER_OFF_NEW] [varchar](50) NULL,
	[US_CLASSIFICATION] [varchar](50) NULL,
	[REGION_CLASSIFICATION] [varchar](50) NULL,
	[MARKET_CLASSIFICATION] [varchar](50) NULL,
	[MHUSA_SUB_CHANNEL] [varchar](50) NULL,
	[PROFILES] [varchar](50) NULL,
	[ACCOUNT_OWNER] [varchar](50) NULL,
	[SEGMENTATION] [varchar](50) NULL,
	[STORE_TYPE] [varchar](50) NULL,
	[E_RETAILER] [varchar](50) NULL,
	[PURE_PLAYER] [varchar](50) NULL,
	[DMA_NAME] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_NABCA_SAM_STORE_DIM_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
