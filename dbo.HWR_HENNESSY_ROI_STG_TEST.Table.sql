USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_HENNESSY_ROI_STG_TEST]    Script Date: 9/28/2019 6:52:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_HENNESSY_ROI_STG_TEST]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_HENNESSY_ROI_STG_TEST](
	[REGION] [varchar](50) NULL,
	[STATE] [varchar](50) NULL,
	[ACCOUNT_NAME] [varchar](150) NULL,
	[TDLINX] [varchar](50) NULL,
	[SOW] [varchar](200) NULL,
	[ZIP] [varchar](50) NULL,
	[ACCOUNT_CHANNEL] [varchar](50) NULL,
	[ACCOUNT_SPEND] [numeric](16, 10) NULL,
	[STAFF_PAYROLL_TOTAL] [numeric](16, 10) NULL,
	[SUBTOTAL] [numeric](16, 10) NULL,
	[AGENCY_FEE] [numeric](16, 10) NULL,
	[TOTAL] [numeric](16, 10) NULL,
	[Q1_2017] [numeric](16, 10) NULL,
	[Q2_2017] [numeric](16, 10) NULL,
	[Q1_2018] [numeric](16, 10) NULL,
	[Q2_2018] [numeric](16, 10) NULL,
	[Q3_2017] [numeric](16, 10) NULL,
	[Q3_2018] [numeric](16, 10) NULL,
	[Q4_2017] [numeric](16, 10) NULL,
	[Q4_2018] [numeric](16, 10) NULL,
	[Q1_2019] [numeric](16, 10) NULL,
	[Q2_2019] [numeric](16, 10) NULL,
	[Q3_2019] [numeric](16, 10) NULL,
	[Q4_2019] [numeric](16, 10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_HENNESSY_ROI_STG_TEST] WITH ROWCOUNT = 7464, PAGECOUNT = 279
GO
