USE [Hennessy_WR]
GO
/****** Object:  Table [dbo].[HWR_SG_BRANDS_INCLUSION_LIST_STG]    Script Date: 9/28/2019 6:52:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HWR_SG_BRANDS_INCLUSION_LIST_STG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HWR_SG_BRANDS_INCLUSION_LIST_STG](
	[MARQUE_DESCRIPTION] [varchar](50) NULL,
	[INCLUSIONLIST] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
UPDATE STATISTICS [dbo].[HWR_SG_BRANDS_INCLUSION_LIST_STG] WITH ROWCOUNT = 0, PAGECOUNT = 0
GO
